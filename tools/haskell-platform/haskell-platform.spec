# https://fedoraproject.org/wiki/Packaging:Haskell
# https://fedoraproject.org/wiki/PackagingDrafts/Haskell

%global ghc_compiler_version 7.4.2
%global alex_version 3.0.2
%global cabal_install_version 0.14.0
%global happy_version 1.18.10

%global primitive_version 0.5.0.1
%global split_version 0.2.1.1
%global vector_version 0.10.0.1

%global ghc_exclude_docdir 1

Name:           haskell-platform
Version:        2012.4.0.0
# Since library subpackages are versioned:
# - release can only be reset if all library versions get bumped simultaneously
#   (eg for a major release)
# - minor release numbers should be incremented monotonically
Release:        22.1%{?dist}
Summary:        Standard Haskell distribution

Group:          Development/Tools
License:        BSD
URL:            http://hackage.haskell.org/platform
Source0:        http://lambda.haskell.org/platform/download/%{version}/%{name}-%{version}.tar.gz
Patch1:         ghc-GLUT-extralibs.patch
Patch2:         alex-ghc74-fix-bang-pattern.patch

BuildRequires:  ghc-Cabal-devel
BuildRequires:  ghc-rpm-macros
# for fixing RPATH in the binary programs
BuildRequires:  chrpath
BuildRequires:  ghc-compiler = %{ghc_compiler_version}
# GLUT
BuildRequires:  freeglut-devel%{?_isa}
# OpenGL
BuildRequires:  mesa-libGL-devel%{?_isa},mesa-libGLU-devel%{?_isa}
# QuickCheck
BuildRequires:  ghc-extensible-exceptions-devel
%ifarch %{ghc_arches_with_ghci}
BuildRequires:  ghc-template-haskell-devel
%endif
# haskell-src
BuildRequires:  ghc-haskell98-devel
# parallel, text
BuildRequires:  ghc-deepseq-devel
# random
BuildRequires:  ghc-time-devel
# regex-base, text
BuildRequires:  ghc-bytestring-devel
# zlib
BuildRequires:  zlib-devel%{?_isa}
# part of HP-2012.4
BuildRequires:  ghc-split-devel = %{split_version}
BuildRequires:  ghc-primitive-devel = %{primitive_version}
BuildRequires:  ghc-vector-devel = %{vector_version}
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
# ghci "ghc" library is not officially part of hackage-platform
Requires:       ghc-compiler = %{ghc_compiler_version}
Requires:       alex = %{alex_version}-%{release}
Requires:       cabal-install = %{cabal_install_version}-%{release}
Requires:       happy = %{happy_version}-%{release}
Requires:       ghc-haskell-platform-devel = %{version}-%{release}

%description
Haskell Platform is a suite of stable and well used Haskell libraries
and tools.  It provides a good starting environment for Haskell development.


%global BSDHaskellReport BSD%{space}and%{space}HaskellReport

%global haskell_platform_version %{version}

# follows order of platform.packages
%if %{defined ghclibdir}
%ghc_lib_subpackage HUnit 1.2.5.1
%ghc_lib_subpackage -c mesa-libGL-devel%{?_isa},mesa-libGLU-devel%{?_isa} OpenGL 2.2.3.1
%ghc_lib_subpackage -c freeglut-devel%{?_isa} GLUT 2.1.2.1
%ghc_lib_subpackage html 1.0.1.2
%ghc_lib_subpackage parallel 3.2.0.3
%ghc_lib_subpackage -l %BSDHaskellReport random 1.0.1.1
%ghc_lib_subpackage QuickCheck 2.5.1.1
# alex
%ghc_lib_subpackage stm 2.4
%ghc_lib_subpackage async 2.0.1.3
%ghc_lib_subpackage syb 0.3.7
%ghc_lib_subpackage haskell-src 1.0.1.5
%ghc_lib_subpackage text 0.11.2.3
%ghc_lib_subpackage transformers 0.3.0.0
%ghc_lib_subpackage mtl 2.1.2
%ghc_lib_subpackage fgl 5.4.2.4
# happy
%ghc_lib_subpackage parsec 3.1.3
%ghc_lib_subpackage network 2.3.1.0
%ghc_lib_subpackage HTTP 4000.2.5
%ghc_lib_subpackage regex-base 0.93.2
%ghc_lib_subpackage regex-posix 0.95.2
%ghc_lib_subpackage regex-compat 0.95.1
%ghc_lib_subpackage xhtml 3000.2.1
%ghc_lib_subpackage cgi 3001.1.7.4
%ghc_lib_subpackage -c zlib-devel%{?_isa} zlib 0.5.4.0
# cabal-install
%endif

%package -n alex
Version:        %{alex_version}
Summary:        Lexer generator for Haskell
Group:          Development/Tools
License:        BSD
URL:            http://hackage.haskell.org/package/alex
BuildRequires:  autoconf docbook-style-xsl libxslt
%ifarch ppc ppc64 s390 s390x
# for the BangPatterns patch
BuildRequires:  alex
%endif

%description -n alex
Alex is a tool for generating lexical analysers in Haskell from a
description of the tokens to be recognised in the form of regular
expressions.  It is similar to the tool lex or flex for C/C++.


%package -n cabal-install
Version:        %{cabal_install_version}
Summary:        Haskell package tool
Group:          Development/Tools
License:        BSD
URL:            http://hackage.haskell.org/package/%{name}
Source1:        cabal-install.sh
Requires:       ghc-compiler

%description -n cabal-install
The 'cabal' command-line program simplifies the process of managing Haskell
packages by automating fetching, configuration, compilation and
installation of Haskell libraries and programs from Hackage.


%package -n happy
Version:        %{happy_version}
Summary:        LALR(1) Parser Generator for Haskell
License:        BSD
Group:          Development/Tools
URL:            http://hackage.haskell.org/package/happy
BuildRequires:  autoconf, docbook-dtds, docbook-style-xsl, libxslt, libxml2

%description -n happy
Happy is a parser generator system for Haskell, similar to the tool
`yacc' for C. Like `yacc', it takes a file containing an annotated BNF
specification of a grammar and produces a Haskell module containing a
parser for the grammar.

Happy is flexible: you can have several Happy parsers in the same
program, and several entry points to a single grammar. Happy can work
in conjunction with a lexical analyser supplied by the user (either
hand-written or generated by another program).


%global version %{haskell_platform_version}

%ghc_devel_package -m haskell-platform %{version}
%{?ghc_packages_list:Requires: %(echo %{ghc_packages_list} | sed -e "s/\([^ ]*\)-\([^ ]*\)/ghc-\1-devel = \2-%{release},/g")}
Requires:       ghc-libraries = %{ghc_compiler_version}
# part of HP-2012.4
Requires:       ghc-primitive-devel = %{primitive_version}
Requires:       ghc-split-devel = %{split_version}
Requires:       ghc-vector-devel = %{vector_version}
# added in F17 devel cycle
Obsoletes:      ghc-haskell-platform < %{version}-%{release}

%ghc_devel_description haskell-platform
Haskell Platform is a suite of stable and well used Haskell libraries
and tools.  It provides a good starting environment for Haskell development.


%prep
%setup -q -n %{name}-%{version}

cd packages/GLUT-*
%patch1 -p1 -b .orig

%ifarch ppc ppc64 s390 s390x
cd ../alex-%{alex_version}
%patch2 -p1 -b .orig
%endif

cd ../..


%build
HOME=$PWD
PATH=$HOME/.cabal/bin:$PATH
#%%define cabal_configure_options --global "--package-db=../../packages/package.conf.inplace"  "--ghc-pkg-option=--package-conf=../../packages/package.conf.inplace"
#%%define cabal_configure_options --package-conf=../package.conf.d
%define cabal_configure_options --user

cd packages
for i in $(egrep -v "^(primitive|split|vector)-" platform.packages); do
name=$(echo $i | sed -e "s/\(.*\)-.*/\1/")
ver=$(echo $i | sed -e "s/.*-\(.*\)/\1/")
cd $name-$ver
case $name in
alex|cabal-install|happy)
%ghc_bin_build
;;
# ghc-7.4.1 haddock breaks on GLUT
GLUT)
%ghc_lib_build_without_haddock $name $ver
./Setup register --inplace
;;
haskell-platform)
cabal_configure_extra_options="--with-cabal-install=../cabal-install-%{cabal_install_version}/dist/build/cabal/cabal --with-happy=../happy-%{happy_version}/dist/build/happy/happy  --with-alex=../alex-%{alex_version}/dist/build/alex/alex"
%ghc_lib_build_without_haddock $name $ver
cabal_configure_extra_options=
;;
*)
%ghc_lib_build $name $ver
./Setup register --inplace
;;
esac
cd ..
done

# build alex documentation
for i in alex-%{alex_version} happy-%{happy_version}; do
cd $i/doc
autoreconf
%configure
make html
cd ../..
done

%install
HOME=$PWD
PATH=$HOME/.cabal/bin:$PATH

cd packages

PACKAGES_DIR=$PWD

for i in $(egrep -v "^(primitive|split|vector)-" platform.packages); do
name=$(echo $i | sed -e "s/\(.*\)-.*/\1/")
ver=$(echo $i | sed -e "s/.*-\(.*\)/\1/")
cd $name-$ver
case $name in
alex|cabal-install|happy)
%ghc_bin_install $name $ver
;;
*)
%ghc_lib_install $name $ver
echo "%doc packages/$name-$ver/LICENSE" >> ghc-$name%{?ghc_without_shared:-devel}.files
;;
esac
cd ..
done

cd cabal-install-%{cabal_install_version}
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/bash_completion.d
cp -p bash-completion/cabal $RPM_BUILD_ROOT%{_sysconfdir}/bash_completion.d
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/profile.d
install -pm 644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/profile.d
cd ..

%ghc_strip_dynlinked

# haskell-platform
if [ -d %{buildroot}%{_docdir}/ghc-haskell-platform-%{version} ]; then
  mv %{buildroot}%{_docdir}/ghc-haskell-platform{,-devel}-%{version}
fi

mv %{buildroot}%{_datadir}/HUnit-*/* %{buildroot}%{_docdir}/ghc-HUnit-*/

mv */*.files ..
cd ..

# fix RPATHs in programs linking to HP user libs
%if %{undefined ghc_without_dynamic}
for i in alex cabal happy; do
  PROG=%{buildroot}%{_bindir}/$i
  RPATH=$(chrpath $PROG| sed -e "s!^$PROG: RPATH=!!")
  case $RPATH in
    *$PACKAGES_DIR*)
      NEWRPATH=$(echo $RPATH | sed -e "s!$PACKAGES_DIR!%{ghclibdir}!g" -e "s!/dist/build!!g")
      chrpath -r $NEWRPATH $PROG
      ;;
  esac
done
%endif


%ghc_devel_post_postun haskell-platform


%files
%doc packages/%{name}-%{version}/LICENSE


# protect for koji
%if %{defined ghc_devel_files}
%files -n ghc-haskell-platform-devel -f ghc-haskell-platform-devel.files
%doc packages/haskell-platform-%{version}/LICENSE
%endif


%files -n alex
%doc packages/alex-%{alex_version}/ANNOUNCE
%doc packages/alex-%{alex_version}/LICENSE
%doc packages/alex-%{alex_version}/README
%doc packages/alex-%{alex_version}/TODO
#%doc packages/alex-%{alex_version}/doc/alex
%doc packages/alex-%{alex_version}/examples
%{_bindir}/alex
%{_datadir}/alex-%{alex_version}


%files -n cabal-install
%doc packages/cabal-install-%{cabal_install_version}/LICENSE
%doc packages/cabal-install-%{cabal_install_version}/README
%{_bindir}/cabal
%{_sysconfdir}/bash_completion.d
%{_sysconfdir}/profile.d/cabal-install.sh


%files -n happy
%doc packages/happy-%{happy_version}/ANNOUNCE
%doc packages/happy-%{happy_version}/CHANGES
%doc packages/happy-%{happy_version}/LICENSE
%doc packages/happy-%{happy_version}/README
%doc packages/happy-%{happy_version}/TODO
%doc packages/happy-%{happy_version}/doc/happy
%{_bindir}/happy
%{_datadir}/happy-%{happy_version}


%changelog
* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2012.4.0.0-22
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Thu Dec  6 2012 Jens Petersen <petersen@redhat.com> - 2012.4.0.0-21
- vector was patched to build on all archs (#883479)

* Wed Dec  5 2012 Jens Petersen <petersen@redhat.com> - 2012.4.0.0-20
- keep split, vector, and primitive in their own existing src packages
- allow building on ghc archs without ghci: ie without vector library (#883479)

* Sat Oct 20 2012 Jens Petersen <petersen@redhat.com> - 2012.4.0.0-19
- update to 2012.4.0.0
- new subpackages: async, split, vector, and primitive (vector dep)
- drop explicit BR hscolour

* Mon Jul 23 2012 Jens Petersen <petersen@redhat.com> - 2012.2.0.0-18
- also apply the alex fix-bang-pattern patch for s390 and s390x

* Thu Jul 19 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2012.2.0.0-17
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Mon Jul 16 2012 Jens Petersen <petersen@redhat.com> - 2012.2.0.0-16
- change prof BRs to devel

* Thu Jun  7 2012 Jens Petersen <petersen@redhat.com> - 2012.2.0.0-15
- update to 2012.2.0.0
- build the whole of haskell-platform now from this package
  and subpackage like ghc's libraries
- add alex fix-bang-pattern.diff patch from Debian to fix build on ppc archs
  - requires BR alex
- drop common_summary and common_description for subpackaging
- no longer need to unset debug_package
- make sure all the dynamically linked files get stripped
- needs ghc-rpm-macros 0.95.2 or later to build
- use chrpath to fix the program RPATHs when dynamically linked to HP libs

* Wed May  9 2012 Jens Petersen <petersen@redhat.com> - 2011.4.0.741-2
- update cabal-install to 0.14.0

* Sat Mar 24 2012 Jens Petersen <petersen@redhat.com> - 2011.4.0.741-1
- update to ghc-7.4.1 and latest libraries
- temporarily just a meta-package

* Wed Mar 21 2012 Jens Petersen <petersen@redhat.com> - 2011.4.0.0-7
- require ghc-compiler instead of ghc to avoid the ghc lib

* Fri Jan 20 2012 Jens Petersen <petersen@redhat.com> - 2011.4.0.0-6
- update to cabal2spec-0.25.2

* Thu Jan 19 2012 Jens Petersen <petersen@redhat.com> - 2011.4.0.0-5
- update the description

* Thu Jan 19 2012 Jens Petersen <petersen@redhat.com> - 2011.4.0.0-4
- update the source url

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2011.4.0.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Sun Jan  1 2012 Jens Petersen <petersen@redhat.com> - 2011.4.0.0-2
- define ghc_without_shared since ghc-haskell-platform-devel no longer
  requires ghc-haskell-platform

* Wed Dec 28 2011 Jens Petersen <petersen@redhat.com> - 2011.4.0.0-1
- update to 2011.4.0.0
- reenable ppc64
- drop ghc-haskell-platform subpackage
- require ghc-libraries instead of ghc-devel

* Tue Jun 21 2011 Jens Petersen <petersen@redhat.com> - 2011.2.0.1-2
- ghc_arches replaces ghc_excluded_archs (cabal2spec-0.23.2)

* Mon Jun 20 2011 Jens Petersen <petersen@redhat.com> - 2011.2.0.1-1
- update to 2011.2.0.1: ghc-7.0.3 and text-0.11.0.6
- update source url
- use ghc_excluded_archs
- exclude ppc64: no QuickCheck
- bump ghc to 7.0.4
- use top_prefix for path to haskell-platform subdir in large tarball
- drop upstream_version

* Fri May 27 2011 Jens Petersen <petersen@redhat.com> - 2011.2.0.0-5
- drop the prof subpackage

* Wed May 25 2011 Jens Petersen <petersen@redhat.com> - 2011.2.0.0-4
- add ppc64 arch

* Mon Mar 28 2011 Jens Petersen <petersen@redhat.com> - 2011.2.0.0-3
- remove duplicate license file from ghc-haskell-platform

* Mon Mar 28 2011 Jens Petersen <petersen@redhat.com> - 2011.2.0.0-2
- fix the install scripts:
- ghc_reindex_haddock is now redundant
- use ghc_pkg_recache

* Fri Mar 11 2011 Jens Petersen <petersen@redhat.com> - 2011.2.0.0-1
- 2011.2.0.0 final

* Thu Mar 10 2011 Fabio M. Di Nitto <fdinitto@redhat.com> - 2011.1.0.0-0.6
- Enable build on sparcv9

* Tue Feb 15 2011 Jens Petersen <petersen@redhat.com> - 2011.1.0.0-0.5
- update to latest haskell-platform-2011.1 snapshot

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2011.1.0.0-0.4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sat Jan 22 2011 Jens Petersen <petersen@redhat.com> - 2011.1.0.0-0.3
- make ghc-haskell-platform-devel require ghc-devel and ghc_devel_requires
- build with ghc_lib_build and without_haddock

* Tue Jan 18 2011 Jens Petersen <petersen@redhat.com> - 2011.1.0.0-0.2
- update to cabal2spec-0.22.4

* Sun Dec  5 2010 Jens Petersen <petersen@redhat.com> - 2011.1.0.0-0.1
- update to 2011.1.0.0 alpha snapshot

* Fri Nov 26 2010 Jens Petersen <petersen@redhat.com> - 2010.2.0.0.701-1
- bump some versions for ghc-7.0.1
- add hscolour
- no haddock documentation to build
- remove duplicate LICENSE file

* Fri Jul 23 2010 Jens Petersen <petersen@redhat.com> - 2010.2.0.0-1
- update to 2010.2.0.0 final release (no actual changes)

* Sun Jul 18 2010 Jens Petersen <petersen@redhat.com> - 2010.2.0.0-0.1
- drop debuginfo again: ghc_strip_dynlinked got fixed in ghc-rpm-macros-0.8.1

* Fri Jul 16 2010 Jens Petersen <petersen@redhat.com> - 2010.2.0.0-0.1
- update to 2010.2.0.0 RC
- obsolete ghc-haskell-platform-doc in line with ghc-rpm-macros-0.8.0
- add License to base library too

* Sun Jun 27 2010 Jens Petersen <petersen@redhat.com> - 2010.1.0.0.6123-1
- bump ghc to 6.12.3
- sync cabal2spec-0.22.1
- enable debugging for now to avoid empty strip error

* Thu Apr 29 2010 Jens Petersen <petersen@redhat.com> - 2010.1.0.0.6122-1
- break haskell-platform-2010.1.0.0 with ghc-6.12.2

* Wed Mar 24 2010 Jens Petersen <petersen@redhat.com> - 2010.1.0.0-1
- update to 2010.1.0.0 beta release
- update versions of alex, cgi, network, parallel, QuickCheck, HTTP
- new deepseq dep (#576482)

* Thu Jan 28 2010 Jens Petersen <petersen@redhat.com> - 2009.3.1.20100115-0.2
- add filelist for shared libs
- update devel post and postun

* Sat Jan 16 2010 Jens Petersen <petersen@redhat.com> - 2009.3.1.20100115-0.1
- update to darcs snapshot patched for ghc-6.12.1
- update to ghc-rpm-macros-0.5.1 and cabal2spec-0.21.1:
- drop doc and prof bcond
- use common_summary and common_description
- use ghc_lib_package and ghc_pkg_deps
- build shared library
- drop redundant buildroot and its install cleaning

* Mon Sep 28 2009 Jens Petersen <petersen@redhat.com> - 2009.2.0.2-3
- fix rpmlint warnings (bos, #523883)

* Mon Sep 28 2009 Jens Petersen <petersen@redhat.com> - 2009.2.0.2-2
- add all the buildrequires (#523883)
- create ghcpkgdir since metapackage
- nothing in bindir

* Thu Sep 17 2009 Jens Petersen <petersen@redhat.com> - 2009.2.0.2-1
- initial packaging for Fedora
