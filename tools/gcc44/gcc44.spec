Summary: Dummy gcc44 package for RHEL6
Name: gcc44
Version: 1
Release: 1
License: GPlv3+
Requires: gcc
BuildArch: noarch

%description
This is a dummy package to allow installing
development packages that require RHEL5 gcc44.

%prep
%{nil}

%build
%{nil}

%install
mkdir -p %{buildroot}%{_bindir}
ln -s gcc %{buildroot}%{_bindir}/gcc44

%files
%defattr(-,root,root,-)
%{_bindir}/gcc44

%changelog
* Sun Feb  2 2014 Jens Petersen <petersen@fedoraproject.org> - 1-1
- Initial build.
