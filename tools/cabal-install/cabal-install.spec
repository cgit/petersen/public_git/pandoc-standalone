Name:           cabal-install
Version:        1.18.0.2
Release:        1%{?dist}
Summary:        The command-line interface for Cabal and Hackage

License:        BSD
Group:          Development/Tools
URL:            http://hackage.haskell.org/package/%{name}
Source0:        http://hackage.haskell.org/packages/archive/%{name}/%{version}/%{name}-%{version}.tar.gz
Patch0:         cabal-install-1.18.0.2-bootstrap-hackage-url.patch

BuildRequires:  ghc-Cabal-devel
#BuildRequires:  ghc-rpm-macros
# Begin cabal-rpm deps:
#BuildRequires:  ghc-HTTP-devel
BuildRequires:  ghc-array-devel
BuildRequires:  ghc-bytestring-devel
BuildRequires:  ghc-containers-devel
BuildRequires:  ghc-directory-devel
BuildRequires:  ghc-filepath-devel
#BuildRequires:  ghc-mtl-devel
#BuildRequires:  ghc-network-devel
BuildRequires:  ghc-old-time-devel
BuildRequires:  ghc-pretty-devel
BuildRequires:  ghc-process-devel
#BuildRequires:  ghc-random-devel
#BuildRequires:  ghc-stm-devel
BuildRequires:  ghc-time-devel
BuildRequires:  ghc-unix-devel
#BuildRequires:  ghc-zlib-devel
# End cabal-rpm deps
BuildRequires:  zlib-devel%{?_isa}
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root

%description
The 'cabal' command-line program simplifies the process of managing Haskell
software by automating the fetching, configuration, compilation and
installation of Haskell libraries and programs.

The program is statically linked to Haskell libraries.


%global cabal ./dist/build/cabal/cabal


%prep
%setup -q
%patch0 -p1 -b .orig


%build
# bootstrap ghc-pkg registers deps 
HOME=$PWD
sh bootstrap.sh
%cabal configure --prefix=%{_prefix} --libdir=%{_libdir} --docdir=%{?_pkgdocdir}%{!?_pkgdocdir:%{_docdir}/%{name}-%{version}}


%install
rm -rf %{buildroot}
%cabal copy --destdir=%{buildroot} -v


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc LICENSE README
%{_bindir}/cabal


%changelog
* Sat Jan 25 2014 Jens Petersen <petersen@redhat.com> - 1.18.0.2-1
- build with bootstrap.sh patched for downloading from hackage2
