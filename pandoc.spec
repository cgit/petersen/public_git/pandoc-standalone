%global pandoc_ver 1.13.1
%global pandoc_citeproc_ver 0.5

# nothing to see here
%global debug_package %{nil}

Name:           pandoc
Version:        %{pandoc_ver}
Release:        1%{?dist}
Summary:        Conversion between markup formats

License:        GPLv2+
Group:          Text/Processing
URL:            http://hackage.haskell.org/package/%{name}
Source0:        http://hackage.haskell.org/package/%{name}/%{version}/%{name}-%{version}.tar.gz
Source1:        http://hackage.haskell.org/package/%{name}-citeproc/%{pandoc_citeproc_ver}/%{name}-citeproc-%{pandoc_citeproc_ver}.tar.gz

BuildRequires:  ghc-Cabal-devel
#BuildRequires:  ghc-rpm-macros
# Begin cabal-rpm deps:
BuildRequires:  alex
#BuildRequires:  chrpath
BuildRequires:  ghc-HTTP-devel
#BuildRequires:  ghc-aeson-devel
#BuildRequires:  ghc-aeson-pretty-devel
BuildRequires:  ghc-array-devel
#BuildRequires:  ghc-attoparsec-devel
#BuildRequires:  ghc-base64-bytestring-devel
BuildRequires:  ghc-binary-devel
#BuildRequires:  ghc-blaze-html-devel
#BuildRequires:  ghc-blaze-markup-devel
BuildRequires:  ghc-bytestring-devel
BuildRequires:  ghc-containers-devel
#BuildRequires:  ghc-data-default-devel
BuildRequires:  ghc-directory-devel
BuildRequires:  ghc-extensible-exceptions-devel
BuildRequires:  ghc-filepath-devel
#BuildRequires:  ghc-hexpat-devel
#BuildRequires:  ghc-highlighting-kate-devel
#BuildRequires:  ghc-hs-bibutils-devel
#BuildRequires:  ghc-hslua-devel
#BuildRequires:  ghc-http-conduit-devel
#BuildRequires:  ghc-http-types-devel
BuildRequires:  ghc-mtl-devel
BuildRequires:  ghc-network-devel
BuildRequires:  ghc-old-locale-devel
BuildRequires:  ghc-old-time-devel
#BuildRequires:  ghc-pandoc-types-devel
BuildRequires:  ghc-parsec-devel
BuildRequires:  ghc-process-devel
BuildRequires:  ghc-random-devel
#BuildRequires:  ghc-rfc5051-devel
BuildRequires:  ghc-split-devel
BuildRequires:  ghc-syb-devel
#BuildRequires:  ghc-tagsoup-devel
#BuildRequires:  ghc-temporary-devel
#BuildRequires:  ghc-texmath-devel
BuildRequires:  ghc-text-devel
BuildRequires:  ghc-time-devel
#BuildRequires:  ghc-unordered-containers-devel
BuildRequires:  ghc-vector-devel
#BuildRequires:  ghc-xml-devel
#BuildRequires:  ghc-yaml-devel
#BuildRequires:  ghc-zip-archive-devel
BuildRequires:  ghc-zlib-devel
BuildRequires:  happy
# End cabal-rpm deps
# uses sandboxing
BuildRequires:  cabal-install
BuildRequires:  hsb2hs
# for execstack
BuildRequires:  prelink
BuildRequires:  pcre-devel%{?_isa}
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root

%description
Pandoc a command-line tool that can read markdown and (subsets of) HTML,
reStructuredText, LaTeX, DocBook, MediaWiki markup, Haddock markup,
OPML, and Textile, and it can write markdown, reStructuredText, HTML,
LaTeX, ConTeXt, Docbook, OPML, OpenDocument, ODT, Word docx, RTF, MediaWiki,
Textile, groff man pages, plain text, Emacs Org-Mode, AsciiDoc, EPUB (v2 and
v3), FictionBook2, and several kinds of HTML/javascript slide shows (S5, Slidy,
Slideous, DZSlides, reveal.js).

Pandoc extends standard markdown syntax with footnotes, embedded LaTeX,
definition lists, tables, and other features. A compatibility mode is provided
for those who need a drop-in replacement for Markdown.pl.

In contrast to existing tools for converting markdown to HTML, which use regex
substitutions, pandoc has a modular design: it consists of a set of readers,
which parse text in a given format and produce a native representation of the
document, and a set of writers, which convert this native representation into a
target format. Thus, adding an input or output format requires only adding a
reader or writer.


%package citeproc
Summary:        Supports using pandoc with citeproc
Version:        %{pandoc_citeproc_ver}
Group:          Text/Processing
License:        BSD and GPLv2+

%description citeproc
This package contains an executable: pandoc-citeproc, which works as a
pandoc filter, and also has a mode for converting bibliographic databases
a YAML format suitable for inclusion in pandoc YAML metadata.


%prep
%setup -q -a1


%build
%global cabal cabal
cabal update
%cabal install -f "embed_data_files" pandoc-%{pandoc_ver} pandoc-citeproc-%{pandoc_citeproc_ver} --prefix=$PWD/cabal-dev


%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
install -p cabal-dev/bin/%{name} cabal-dev/bin/%{name}-citeproc %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_mandir}/{man1,man5}
install -p -m 644 man/man1/pandoc.1 pandoc-citeproc-%{pandoc_citeproc_ver}/man/man1/pandoc-citeproc.1 %{buildroot}%{_mandir}/man1
install -p -m 644 man/man5/pandoc_markdown.5 %{buildroot}%{_mandir}/man5


# clear execstack
execstack -c %{buildroot}%{_bindir}/*


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc COPYRIGHT README
%doc cabal-dev/share/doc/*
%{_bindir}/%{name}
%{_mandir}/man1/pandoc.1* 
%{_mandir}/man5/pandoc_markdown.5*


%files citeproc
%defattr(-,root,root,-)
%doc pandoc-citeproc-%{pandoc_citeproc_ver}/README.md
%doc cabal-dev/share/doc/*
%{_bindir}/%{name}-citeproc
%{_mandir}/man1/pandoc-citeproc.1*


%changelog
* Sun Jan  4 2015 Jens Petersen <petersen@redhat.com> - 1.13.1-1
- update to pandoc 1.13.1 and pandoc-citeproc 0.5
- build with "cabal-dev install"

* Sat Aug 30 2014 Jens Petersen <petersen@redhat.com> - 1.12.4.2-1
- update to pandoc-1.12.4.2 and pandoc-citeproc-0.3.1
- do not implicitly assume building latest releases

* Tue Feb  4 2014 Jens Petersen <petersen@redhat.com> - 1.12.3.1-4
- do not statically link C deps, since static glibc is not portable anyway
- clear execstack on executables

* Sun Feb  2 2014 Jens Petersen <petersen@redhat.com> - 1.12.3.1-3
- include pandoc-citeproc-0.3.0.1
- link programs statically
- include licenses of bundled libraries

* Sat Jan 25 2014 Jens Petersen <petersen@redhat.com> - 1.12.3.1-1
- build with cabal-dev and embed_data_files
