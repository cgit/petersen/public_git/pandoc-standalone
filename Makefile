build: pandoc.spec
	rpmbuild -bb pandoc.spec

srpm: pandoc.spec
	rpmbuild -bs pandoc.spec --undefine dist --define "_source_filedigest_algorithm md5"

README.html: README.md
	pandoc README.md > README.html

sync: README.html

RSYNC_OPTIONS = --exclude=.git

-include ../common.mk

CREATEREPO_OPTS = -s sha

DISTS = epel-5
ARCHS = SRPMS i386 x86_64

REPO_PATH = public_html/pandoc-standalone
