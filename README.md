**Latest packages are now at <https://copr.fedoraproject.org/coprs/petersen/pandoc-el5>**

# Pandoc standalone packages

Standalone rpm packages of pandoc and pandoc-citeproc
built on Enterprise Linux 5
which also work on Red Hat Enterprise Linux 6
and older versions of Fedora.

To aid portability the executables are statically linked
against the Haskell dependencies, unlike the packages in
Fedora and EPEL, and are also built with embedded data files
and http-conduit support.

## Download

The binaries can either be:

a) downloaded from <http://petersen.fedorapeople.org/pandoc-standalone/epel-5/>

or

b) if you prefer you can add the yum repo
<http://petersen.fedorapeople.org/pandoc-standalone/pandoc-standalone.repo>
to /etc/yum.repos.d/ and then installed with:

    yum install pandoc pandoc-citeproc

## Build requirements

- ghc-7.4 (pandoc-1.12 needs ghc-7.2 or later to build)
- haskell-platform
- cabal-dev
- hsb2hs (to embed data files)
- pcre-devel
- libffi-devel (from EPEL5)

The current packages were built using ghc-7.4.2
and haskell-platform-2012.4.0.0 with cabal-dev-0.9.1
packages from the repo
<http://repos.fedorapeople.org/repos/petersen/ghc-7.4.2/>
and additionally hsb2hs-0.1 from the
<http://petersen.fedorapeople.org/pandoc-standalone/> repo.

The build from a single spec file needs a network
connection for cabal to download the Haskell libraries dependencies
otherwise they need to be downloaded in advance using
{cabal fetch pandoc-citeproc}.

## Build steps

Here are the general steps to package the packages on RHEL or Fedora.
In principle it should be possible to build everything in one go using
mockchain on the srpm's.

### RHEL 5

    $ su
    # wget http://repos.fedorapeople.org/repos/petersen/ghc-7.4.2/epel-ghc-7.4.2.repo -P /etc/yum.repos.d
    # wget http://petersen.fedorapeople.org/pandoc-standalone/pandoc-standalone.repo -P /etc/yum.repos.d
    # yum install haskell-platform cabal-dev hsb2hs pcre-devel libffi-devel rpm-build
    # 
    $ cabal update    # skip if run recently
    $ rpm -iv http://petersen.fedorapeople.org/pandoc-standalone/epel-5/SRPMS/pandoc-1.12.3.1-4.src.rpm
    $ rpmbuild -bb pandoc.spec

### RHEL 6 or older Fedora

    $ sudo wget http://repos.fedorapeople.org/repos/petersen/ghc-7.4.2/epel-ghc-7.4.2.repo -P /etc/yum.repos.d 
    $ sudo wget http://petersen.fedorapeople.org/pandoc-standalone/pandoc-standalone.repo -P /etc/yum.repos.d 
    $ sudo yum install yum-utils      # if you don't have yum-builddep
    $ sudo yum-builddep ~/rpmbuild/SPECS/pandoc.spec
    $ cabal update    # skip if run recently
    $ rpm -iv http://petersen.fedorapeople.org/pandoc-standalone/epel-5/SRPMS/pandoc-1.12.3.1-4.src.rpm
    $ rpmbuild -bb ~/rpmbuild/SPECS/pandoc.spec

### Fedora 19+

    $ rpm -iv http://petersen.fedorapeople.org/pandoc-standalone/epel-5/SRPMS/hsb2hs-0.1-1.src.rpm
    $ cabal update    # skip if run recently
    $ rpmbuild -bb ~/rpmbuild/SPECS/hsb2hs.spec
    $ sudo rpm -ivh ~/rpmbuild/RPMS/$(arch)/hsb2hs-0*
    $ rpm -iv http://petersen.fedorapeople.org/pandoc-standalone/epel-5/SRPMS/pandoc-1.12.3.1-4.src.rpm
    $ sudo yum install yum-utils      # if you don't have yum-builddep
    $ sudo yum-builddep ~/rpmbuild/SPECS/pandoc.spec
    $ rpmbuild -bb ~/rpmbuild/SPECS/pandoc.spec
    $ sudo rpm -Uvh ~/rpmbuild/RPMS/$(arch)/pandoc-*

or in the dir with pandoc.spec just run:

    $ cabal install hsb2hs
    $ # comment out "Requires: hsb2hs" in pandoc.spec
    $ sudo yum install cabal-rpm
    $ cblrpm rpm pandoc
    $ sudo rpm -Uvh ~/rpmbuild/RPMS/$(arch)/pandoc-*

# Git repo
If you want to build all the packages from scratch up from ghc
you can use this git repo:

    git clone git://fedorapeople.org/~petersen/pandoc-standalone.git

You can browse the repo at:

    http://fedorapeople.org/cgit/petersen/public_git/pandoc-standalone.git/

# Support
If you have questions, patches, or issues with pandoc-standalone
you can send mail to petersen@fedoraproject.org.
